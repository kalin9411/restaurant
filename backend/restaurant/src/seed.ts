import { createConnection, Repository, In } from 'typeorm';
import { Workday } from './models/entities/workday.entity';


const seedWorkdays = async (connection: any) => {
  const workdayRepo: Repository<Workday> = connection.manager.getRepository(Workday);

  const monday = await workdayRepo.findOne({
    where: {
      name: 'Monday',
    },
  });

  if (monday) {
    console.log('The DB already has been seeded');
    return;
  }

  const Monday: Workday = workdayRepo.create({
    name: 'Monday',
    startHour: '10',
    startMinute: '00',
    endHour: '22',
    endMinute: '00'
  });

  await workdayRepo.save(Monday);

  const Tuesday: Workday = workdayRepo.create({
    name: 'Tuesday',
    startHour: '10',
    startMinute: '00',
    endHour: '22',
    endMinute: '00'
  });

  await workdayRepo.save(Tuesday);

  const Wednesday: Workday = workdayRepo.create({
    name: 'Wednesday',
    startHour: '10',
    startMinute: '00',
    endHour: '22',
    endMinute: '00'
  });

  await workdayRepo.save(Wednesday);

  const Thursday: Workday = workdayRepo.create({
    name: 'Thursday',
    startHour: '10',
    startMinute: '00',
    endHour: '22',
    endMinute: '00'
  });

  await workdayRepo.save(Thursday);

  const Friday: Workday = workdayRepo.create({
    isWorking: false,
    name: 'Friday',
    startHour: '10',
    startMinute: '00',
    endHour: '00',
    endMinute: '00'
  });

  await workdayRepo.save(Friday);

   const Saturday: Workday = workdayRepo.create({
    isWorking: false,
    name: 'Saturday',
    startHour: '12',
    startMinute: '00',
    endHour: '20',
    endMinute: '00'
  });

  await workdayRepo.save(Saturday);

  
  const Sunday: Workday = workdayRepo.create({
    isWorking: false,
    name: 'Sunday',
    startHour: '12',
    startMinute: '00',
    endHour: '20',
    endMinute: '00'
  });

  await workdayRepo.save(Sunday);
  console.log('Seeded successfully!');
};

const seed = async () => {
  // console.log('Seed started!');
  const connection = await createConnection();

  await seedWorkdays(connection);

  await connection.close();
};

seed().catch(console.error);
