import { Injectable } from '@nestjs/common';
import CreateTableDTO from './models/CreateTableDTO';

@Injectable()
export class AppService {
  getHello(id): string {
    return `Hello World users! ${id}`;
  }

}
