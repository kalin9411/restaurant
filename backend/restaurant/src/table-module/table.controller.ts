import { Body, Controller, Delete, Get, Param, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import CreateTableDTO from 'src/models/CreateTableDTO';
import { Table } from 'src/models/entities/table.entity';
import { TableService } from './table.service';

@Controller('table')
@UsePipes(new ValidationPipe())
export class TableController {

    constructor(private readonly tableService: TableService) {}

    @Get()
    async getAllTables() {
      return await this.tableService.getAllTables()
    }
      
    @Post()
    async createTable(@Body() body: CreateTableDTO) {
      return await this.tableService.createTable(body)
    }

    @Put('/:tableId')
    async updateTable(@Param('tableId') tableId: string, @Body() body: Table) {
      return await this.tableService.updateTable(tableId, body)
    }

    @Delete('/:tableId')
    async deleteTable(@Param('tableId') tableId: string) {
      return await this.tableService.deleteTable(tableId)
    }
}
