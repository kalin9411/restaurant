import { Module } from '@nestjs/common';
import { TableService } from './table.service';
import { TableController } from './table.controller';
import { Table } from 'src/models/entities/table.entity'
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from 'src/models/entities/reservation.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Table, Reservation])],
  providers: [TableService],
  controllers: [TableController]
})
export class TableModule {}
