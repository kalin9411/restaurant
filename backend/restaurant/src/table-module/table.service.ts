import { BadGatewayException, BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import CreateTableDTO from 'src/models/CreateTableDTO';
import { Table } from 'src/models/entities/table.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TableService {

  constructor(@InjectRepository(Table) private readonly tableRepository: Repository<Table>) {}

    async createTable(table: CreateTableDTO): Promise<Table> {
        return this.tableRepository.save(table)
    }

    async getAllTables():Promise<Table[]> {
      return this.tableRepository.find()
    }

    async updateTable(tableId: string, body: Table): Promise<Table> {
      let table: Table
      try {
        table = await this.tableRepository.findOneOrFail({id: tableId})
      } catch(error) {
        throw new NotFoundException()
      }
      table.name = body.name
      table.maxSeats = body.maxSeats
      return this.tableRepository.save(table)
    }

    async deleteTable(tableId: string) {
      let table: Table
      try {
        table = await this.tableRepository.findOneOrFail({id: tableId})
      } catch(error) {
        throw new NotFoundException()
      }
      return this.tableRepository.delete({id: table.id})
    }
}
