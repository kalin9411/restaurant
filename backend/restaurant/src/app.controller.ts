import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';


@Controller('main')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@Param('id') id: String): string {
    return this.appService.getHello(id);
  }

}
