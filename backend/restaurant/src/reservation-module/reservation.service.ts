import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import CreateReservationDTO from 'src/models/CreateReservationDTO';
import { Reservation } from 'src/models/entities/reservation.entity';
import { Table } from 'src/models/Entities/table.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ReservationService {

    constructor(@InjectRepository(Table) private readonly tableRepository: Repository<Table>,
  @InjectRepository(Reservation) private readonly reservationRepository: Repository<Reservation>) { }

    async createReservation(tableId: string, reservation: Reservation) {
        let table: Table
        try {
        table = await this.tableRepository.findOneOrFail({id: tableId})
        } catch(error) {
            throw new NotFoundException()
        }
        if (reservation.numberOfClients > table.maxSeats) {
            throw new BadRequestException('max seat number exceeded')
        }
        const reserv = await this.reservationRepository.save(reservation) 
        table.reservations.push(reserv)
        return this.tableRepository.save(table)
    }

    async removeReservation(reservationId: string) {
        let reservation: Reservation
        try {
            reservation = await this.reservationRepository.findOneOrFail({id: reservationId})
        } catch(error) {
            throw new NotFoundException();
        }
        return this.reservationRepository.remove(reservation)
    }
}
