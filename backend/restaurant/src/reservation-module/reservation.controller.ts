import { Body, Controller, Delete, Param, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import CreateReservationDTO from 'src/models/CreateReservationDTO';
import { Reservation } from 'src/models/entities/reservation.entity';
import { ReservationService } from './reservation.service';

@Controller('reservation')
@UsePipes(new ValidationPipe())
export class ReservationController {

    constructor(private readonly reservationService: ReservationService) {}

    @Post('/:tableId') 
    async createReservation(@Body() body: Reservation, @Param('tableId') tableId: string) {
        return await this.reservationService.createReservation(tableId, body)
    }

    @Delete('/:reservationId') 
    async removeReservation(@Param('reservationId') reservationId: string) {
        return await this.reservationService.removeReservation(reservationId)
    }
}
