import { Module } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { ReservationController } from './reservation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from 'src/models/entities/reservation.entity';
import { Table } from 'src/models/entities/table.entity'

@Module({
  imports: [TypeOrmModule.forFeature([Table, Reservation])],
  providers: [ReservationService],
  controllers: [ReservationController]
})
export class ReservationModule {}
