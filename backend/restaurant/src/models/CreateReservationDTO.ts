import { IsDate, IsDateString, IsDefined, IsNumber, IsPositive, IsString } from "class-validator"

export default class CreateReservationDTO {

    id: String

    @IsDefined()
    @IsString()
    name: string

    @IsDefined()
    @IsDateString()
    date: Date

    @IsDefined()
    @IsNumber()
    numberOfClients: Number
  }
  