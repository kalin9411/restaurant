import { IsDefined, IsPositive, IsString } from "class-validator"

export default class CreateTableDTO {
    @IsDefined()
    @IsString()
    name: string

    @IsDefined()
    @IsPositive()
    maxSeats: number
    
  }
  