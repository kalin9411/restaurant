
import { Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity('workday')
export class Workday {

    @PrimaryGeneratedColumn('increment')
    id: string

    @Column({default: true})
    isWorking: boolean

    @Column({default: ''})
    name: string

    @Column({default: '00'})
    startHour: string

    @Column({default: '00'})
    startMinute: string

    @Column({default: '00'})
    endHour: string

    @Column({default: '00'})
    endMinute: string

}