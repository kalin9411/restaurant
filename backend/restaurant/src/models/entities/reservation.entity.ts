import { IsPositive, Max } from "class-validator";
import { Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Table } from './table.entity'

@Entity('reservation')
export class Reservation {

    @PrimaryGeneratedColumn('increment')
    id: string

    @Column()
    name: string

    @Column({type: Date})
    date: Date

    @ManyToOne(type => Table, table => table.reservations, {onDelete: 'CASCADE'})
    table: Table

    @Column()
    numberOfClients: Number

}