import { Entity, PrimaryColumn, Column, OneToMany, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { Reservation } from './reservation.entity'

@Entity('table')
export class Table {

    @PrimaryGeneratedColumn('increment')
    id: string

    @Column()
    name: string

    @Column()
    maxSeats: Number

    @OneToMany(type => Reservation, reservation => reservation.table, {eager: true})
    reservations: Reservation[]


}

