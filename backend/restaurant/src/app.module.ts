import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database.module';
import { TableModule } from './table-module/table.module';
import { ReservationModule } from './reservation-module/reservation.module';
import { WorkdayModule } from './workday-module/workday.module';

@Module({
  imports: [DatabaseModule, TableModule, ReservationModule, WorkdayModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
