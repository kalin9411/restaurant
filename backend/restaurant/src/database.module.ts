import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
TypeOrmModule.forRootAsync({
  useFactory: () => ({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '123456123456',
    database: 'Restaurant',
    entities: [__dirname + '/models/entities/*.entity{.ts,.js}'],
    synchronize: true,
    autoLoadEntities: true
  }),
})
]})
export class DatabaseModule {}
