import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Workday } from 'src/models/entities/workday.entity';
import { Repository } from 'typeorm';

@Injectable()
export class WorkdayService {

    constructor(@InjectRepository(Workday) private readonly workdayRepository: Repository<Workday>) {}

    async getSchedule() {
        return this.workdayRepository.find()
    }

    async updateWorkDay(workdayName: string, workday: Workday) {
        let day: Workday
        try {
            day = await this.workdayRepository.findOneOrFail({name: workdayName})
        } catch(error) {
            throw new NotFoundException()
        }
        day.isWorking = workday.isWorking
        day.startHour = workday.startHour
        day.endHour = workday.endHour
        day.startMinute = workday.startMinute
        day.endMinute = workday.endMinute
        return this.workdayRepository.save(day);
    }
}
