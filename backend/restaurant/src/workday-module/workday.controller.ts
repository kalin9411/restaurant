import { Body, Controller, Get, Param, Put } from '@nestjs/common';
import { Workday } from 'src/models/entities/workday.entity';
import { WorkdayService } from './workday.service';

@Controller('workday')
export class WorkdayController {

    constructor(private readonly workdayService: WorkdayService) {}

    @Get('/all')
    async getSchedule() {
      return await this.workdayService.getSchedule()
    }

    @Put('/:workdayName')
    async updateWorkDay(@Param('workdayName') workdayName: string, @Body() body: Workday) {
        return await this.workdayService.updateWorkDay(workdayName, body)
    }
}
