export default class CreateTableDTO {
    name: string;
    maxSeats: number;
}
