import { Reservation } from './reservation.entity';
export declare class Table {
    id: string;
    name: string;
    maxSeats: Number;
    reservations: Reservation[];
}
