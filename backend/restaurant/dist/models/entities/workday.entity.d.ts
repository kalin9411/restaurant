export declare class Workday {
    id: string;
    isWorking: boolean;
    name: string;
    startHour: string;
    startMinute: string;
    endHour: string;
    endMinute: string;
}
