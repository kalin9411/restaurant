import { Table } from './table.entity';
export declare class Reservation {
    id: string;
    name: string;
    date: Date;
    table: Table;
    numberOfClients: Number;
}
