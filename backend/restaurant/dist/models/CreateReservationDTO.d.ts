export default class CreateReservationDTO {
    id: String;
    name: string;
    date: Date;
    numberOfClients: Number;
}
