"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const CreateReservationDTO_1 = require("../models/CreateReservationDTO");
const reservation_entity_1 = require("../models/entities/reservation.entity");
const table_entity_1 = require("../models/Entities/table.entity");
const typeorm_2 = require("typeorm");
let ReservationService = class ReservationService {
    constructor(tableRepository, reservationRepository) {
        this.tableRepository = tableRepository;
        this.reservationRepository = reservationRepository;
    }
    async createReservation(tableId, reservation) {
        let table;
        try {
            table = await this.tableRepository.findOneOrFail({ id: tableId });
        }
        catch (error) {
            throw new common_1.NotFoundException();
        }
        if (reservation.numberOfClients > table.maxSeats) {
            throw new common_1.BadRequestException('max seat number exceeded');
        }
        const reserv = await this.reservationRepository.save(reservation);
        table.reservations.push(reserv);
        return this.tableRepository.save(table);
    }
    async removeReservation(reservationId) {
        let reservation;
        try {
            reservation = await this.reservationRepository.findOneOrFail({ id: reservationId });
        }
        catch (error) {
            throw new common_1.NotFoundException();
        }
        return this.reservationRepository.remove(reservation);
    }
};
ReservationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(table_entity_1.Table)),
    __param(1, typeorm_1.InjectRepository(reservation_entity_1.Reservation)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], ReservationService);
exports.ReservationService = ReservationService;
//# sourceMappingURL=reservation.service.js.map