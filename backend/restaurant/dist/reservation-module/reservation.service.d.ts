import { Reservation } from 'src/models/entities/reservation.entity';
import { Table } from 'src/models/Entities/table.entity';
import { Repository } from 'typeorm';
export declare class ReservationService {
    private readonly tableRepository;
    private readonly reservationRepository;
    constructor(tableRepository: Repository<Table>, reservationRepository: Repository<Reservation>);
    createReservation(tableId: string, reservation: Reservation): Promise<Table>;
    removeReservation(reservationId: string): Promise<Reservation>;
}
