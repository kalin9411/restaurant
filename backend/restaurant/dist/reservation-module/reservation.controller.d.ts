import { Reservation } from 'src/models/entities/reservation.entity';
import { ReservationService } from './reservation.service';
export declare class ReservationController {
    private readonly reservationService;
    constructor(reservationService: ReservationService);
    createReservation(body: Reservation, tableId: string): Promise<import("../models/entities/table.entity").Table>;
    removeReservation(reservationId: string): Promise<Reservation>;
}
