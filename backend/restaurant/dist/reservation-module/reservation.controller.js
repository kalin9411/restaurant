"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationController = void 0;
const common_1 = require("@nestjs/common");
const CreateReservationDTO_1 = require("../models/CreateReservationDTO");
const reservation_entity_1 = require("../models/entities/reservation.entity");
const reservation_service_1 = require("./reservation.service");
let ReservationController = class ReservationController {
    constructor(reservationService) {
        this.reservationService = reservationService;
    }
    async createReservation(body, tableId) {
        return await this.reservationService.createReservation(tableId, body);
    }
    async removeReservation(reservationId) {
        return await this.reservationService.removeReservation(reservationId);
    }
};
__decorate([
    common_1.Post('/:tableId'),
    __param(0, common_1.Body()), __param(1, common_1.Param('tableId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [reservation_entity_1.Reservation, String]),
    __metadata("design:returntype", Promise)
], ReservationController.prototype, "createReservation", null);
__decorate([
    common_1.Delete('/:reservationId'),
    __param(0, common_1.Param('reservationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ReservationController.prototype, "removeReservation", null);
ReservationController = __decorate([
    common_1.Controller('reservation'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __metadata("design:paramtypes", [reservation_service_1.ReservationService])
], ReservationController);
exports.ReservationController = ReservationController;
//# sourceMappingURL=reservation.controller.js.map