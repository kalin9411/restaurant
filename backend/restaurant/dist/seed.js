"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const workday_entity_1 = require("./models/entities/workday.entity");
const seedWorkdays = async (connection) => {
    const workdayRepo = connection.manager.getRepository(workday_entity_1.Workday);
    const monday = await workdayRepo.findOne({
        where: {
            name: 'Monday',
        },
    });
    if (monday) {
        console.log('The DB already has been seeded');
        return;
    }
    const Monday = workdayRepo.create({
        name: 'Monday',
        startHour: '10',
        startMinute: '00',
        endHour: '22',
        endMinute: '00'
    });
    await workdayRepo.save(Monday);
    const Tuesday = workdayRepo.create({
        name: 'Tuesday',
        startHour: '10',
        startMinute: '00',
        endHour: '22',
        endMinute: '00'
    });
    await workdayRepo.save(Tuesday);
    const Wednesday = workdayRepo.create({
        name: 'Wednesday',
        startHour: '10',
        startMinute: '00',
        endHour: '22',
        endMinute: '00'
    });
    await workdayRepo.save(Wednesday);
    const Thursday = workdayRepo.create({
        name: 'Thursday',
        startHour: '10',
        startMinute: '00',
        endHour: '22',
        endMinute: '00'
    });
    await workdayRepo.save(Thursday);
    const Friday = workdayRepo.create({
        isWorking: false,
        name: 'Friday',
        startHour: '10',
        startMinute: '00',
        endHour: '00',
        endMinute: '00'
    });
    await workdayRepo.save(Friday);
    const Saturday = workdayRepo.create({
        isWorking: false,
        name: 'Saturday',
        startHour: '12',
        startMinute: '00',
        endHour: '20',
        endMinute: '00'
    });
    await workdayRepo.save(Saturday);
    const Sunday = workdayRepo.create({
        isWorking: false,
        name: 'Sunday',
        startHour: '12',
        startMinute: '00',
        endHour: '20',
        endMinute: '00'
    });
    await workdayRepo.save(Sunday);
    console.log('Seeded successfully!');
};
const seed = async () => {
    const connection = await typeorm_1.createConnection();
    await seedWorkdays(connection);
    await connection.close();
};
seed().catch(console.error);
//# sourceMappingURL=seed.js.map