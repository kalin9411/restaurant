import { Workday } from 'src/models/entities/workday.entity';
import { Repository } from 'typeorm';
export declare class WorkdayService {
    private readonly workdayRepository;
    constructor(workdayRepository: Repository<Workday>);
    getSchedule(): Promise<Workday[]>;
    updateWorkDay(workdayName: string, workday: Workday): Promise<Workday>;
}
