"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkdayService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const workday_entity_1 = require("../models/entities/workday.entity");
const typeorm_2 = require("typeorm");
let WorkdayService = class WorkdayService {
    constructor(workdayRepository) {
        this.workdayRepository = workdayRepository;
    }
    async getSchedule() {
        return this.workdayRepository.find();
    }
    async updateWorkDay(workdayName, workday) {
        let day;
        try {
            day = await this.workdayRepository.findOneOrFail({ name: workdayName });
        }
        catch (error) {
            throw new common_1.NotFoundException();
        }
        day.isWorking = workday.isWorking;
        day.startHour = workday.startHour;
        day.endHour = workday.endHour;
        day.startMinute = workday.startMinute;
        day.endMinute = workday.endMinute;
        return this.workdayRepository.save(day);
    }
};
WorkdayService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(workday_entity_1.Workday)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], WorkdayService);
exports.WorkdayService = WorkdayService;
//# sourceMappingURL=workday.service.js.map