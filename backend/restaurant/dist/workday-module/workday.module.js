"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkdayModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const workday_entity_1 = require("../models/entities/workday.entity");
const workday_controller_1 = require("./workday.controller");
const workday_service_1 = require("./workday.service");
let WorkdayModule = class WorkdayModule {
};
WorkdayModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([workday_entity_1.Workday])],
        controllers: [workday_controller_1.WorkdayController],
        providers: [workday_service_1.WorkdayService],
    })
], WorkdayModule);
exports.WorkdayModule = WorkdayModule;
//# sourceMappingURL=workday.module.js.map