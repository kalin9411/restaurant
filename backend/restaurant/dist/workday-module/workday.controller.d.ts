import { Workday } from 'src/models/entities/workday.entity';
import { WorkdayService } from './workday.service';
export declare class WorkdayController {
    private readonly workdayService;
    constructor(workdayService: WorkdayService);
    getSchedule(): Promise<Workday[]>;
    updateWorkDay(workdayName: string, body: Workday): Promise<Workday>;
}
