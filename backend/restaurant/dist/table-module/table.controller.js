"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableController = void 0;
const common_1 = require("@nestjs/common");
const CreateTableDTO_1 = require("../models/CreateTableDTO");
const table_entity_1 = require("../models/entities/table.entity");
const table_service_1 = require("./table.service");
let TableController = class TableController {
    constructor(tableService) {
        this.tableService = tableService;
    }
    async getAllTables() {
        return await this.tableService.getAllTables();
    }
    async createTable(body) {
        return await this.tableService.createTable(body);
    }
    async updateTable(tableId, body) {
        return await this.tableService.updateTable(tableId, body);
    }
    async deleteTable(tableId) {
        return await this.tableService.deleteTable(tableId);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TableController.prototype, "getAllTables", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CreateTableDTO_1.default]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "createTable", null);
__decorate([
    common_1.Put('/:tableId'),
    __param(0, common_1.Param('tableId')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, table_entity_1.Table]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "updateTable", null);
__decorate([
    common_1.Delete('/:tableId'),
    __param(0, common_1.Param('tableId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "deleteTable", null);
TableController = __decorate([
    common_1.Controller('table'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __metadata("design:paramtypes", [table_service_1.TableService])
], TableController);
exports.TableController = TableController;
//# sourceMappingURL=table.controller.js.map