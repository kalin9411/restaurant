import CreateTableDTO from 'src/models/CreateTableDTO';
import { Table } from 'src/models/entities/table.entity';
import { Repository } from 'typeorm';
export declare class TableService {
    private readonly tableRepository;
    constructor(tableRepository: Repository<Table>);
    createTable(table: CreateTableDTO): Promise<Table>;
    getAllTables(): Promise<Table[]>;
    updateTable(tableId: string, body: Table): Promise<Table>;
    deleteTable(tableId: string): Promise<import("typeorm").DeleteResult>;
}
