import CreateTableDTO from 'src/models/CreateTableDTO';
import { Table } from 'src/models/entities/table.entity';
import { TableService } from './table.service';
export declare class TableController {
    private readonly tableService;
    constructor(tableService: TableService);
    getAllTables(): Promise<Table[]>;
    createTable(body: CreateTableDTO): Promise<Table>;
    updateTable(tableId: string, body: Table): Promise<Table>;
    deleteTable(tableId: string): Promise<import("typeorm").DeleteResult>;
}
