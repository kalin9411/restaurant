import { Component, OnInit } from '@angular/core';
import ReservationDTO from '../../models/reservationDTO';
import Table from '../../models/table';
import { TableService } from '../../services/table.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {

  tables: Table[];

  constructor(private readonly tableService: TableService) { }

  ngOnInit(): void {
    this.tableService.tables.subscribe(data => {
      this.tables = data;
    });

  }

  sendReservation(event: {table: Table, reservation: ReservationDTO}) {
    this.tableService.addReservation(event.table, event.reservation).subscribe(data => {
      this.tableService.fetchTables();
    });
  }

  removeReservation(event: {reservationId: string}) {
    this.tableService.removeReservation(event.reservationId).subscribe(data => {
      this.tableService.fetchTables();
    });
  }

}
