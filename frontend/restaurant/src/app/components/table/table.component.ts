import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import Table from '../../models/table';
import * as moment from 'moment';
import Reservation from '../../models/reservation';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  filteredReservations: Reservation[];

  @Input()
  table: Table;

  @Output()
  reservationAddEvent = new EventEmitter<object>();

  @Output()
  reservationRemoveEvent = new EventEmitter<object>();

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.filteredReservations = this.table.reservations.filter(
      reservation =>
      reservation.date > moment(new Date()).format('YYYY-MM-DDTHH:mm:00.000z')).sort((a, b) => Date.parse(a.date) - Date.parse(b.date));
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      height: '400px',
      width: '600px',
      data: this.table
    });
    dialogRef.afterClosed().subscribe(result => {
      this.reservationAddEvent.emit({table: this.table, reservation: result});
    });

  }

  removeReservation(event: {reservationId: string}) {
    this.reservationRemoveEvent.emit(event);
  }


}
