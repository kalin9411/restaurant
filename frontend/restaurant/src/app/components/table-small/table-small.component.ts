import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Table from '../../models/table';

@Component({
  selector: 'app-table-small',
  templateUrl: './table-small.component.html',
  styleUrls: ['./table-small.component.css']
})
export class TableSmallComponent implements OnInit {

  constructor() { }

  @Input()
  table: Table;

  @Output()
  updateTableEvent = new EventEmitter<object>();

  @Output()
  deleteTableEvent = new EventEmitter<object>();



  ngOnInit(): void {
  }

  updateTable() {
    this.updateTableEvent.emit(this.table);
  }

  deleteTable() {
    this.deleteTableEvent.emit(this.table);
  }

}
