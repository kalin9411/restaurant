import { Component, OnInit } from '@angular/core';
import Table from '../../models/table';
import { Workday } from '../../models/workDay';
import { TableService } from '../../services/table.service';
import { WorktimeService } from '../../services/worktime.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private readonly tableService: TableService, private readonly worktimeService: WorktimeService) { }

  tables: Table[];
  hours = [];
  minutes = ['00', '15', '30', '45'];
  workTime: {
    Monday: Workday,
    Tuesday: Workday,
    Wednesday: Workday,
    Thursday: Workday,
    Friday: Workday,
    Saturday: Workday,
    Sunday: Workday
  } = {} as any;


  tableName: string;
  tableMaxSeats: number;

  ngOnInit(): void {
    for (let i = 0; i < 24; i++) {
      if (i < 10) {
        this.hours.push(`0${i}`);
      } else {
      this.hours.push(`${i}`);
      }
    }
    this.tableService.tables.subscribe(data => {
      this.tables = data;
    });
    this.worktimeService.workingTime.subscribe(data => {
      data.forEach(day => {
        this.workTime[day.name] = day;
      });
    });

  }

  addTable() {
    this.tableService.addTable({name: this.tableName, maxSeats: this.tableMaxSeats}).subscribe(data => {
      this.tableService.fetchTables();
    });
  }

  updateTable(event: Table) {
    this.tableService.updateTable(event).subscribe(data => {
      this.tableService.fetchTables();
    });
  }

  deleteTable(event: Table) {
    this.tableService.deleteTable(event).subscribe(data => {
      this.tableService.fetchTables();
    });
  }

  updateWorkingTime() {
    this.worktimeService.updateWorkingTime(this.workTime);
  }
}
