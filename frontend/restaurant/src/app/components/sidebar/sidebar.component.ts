import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';
import Table from '../../models/table';
import { TableService } from '../../services/table.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(public dialog: MatDialog, private readonly tableService: TableService, readonly router: Router) {}

  tables: Table[];

  ngOnInit(): void {
    this.tableService.tables.subscribe(data => {
      this.tables = data;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {data: this.tables, width: '600'});
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.tableService.addReservation(result.table, result.reservation).subscribe(data => {
          this.tableService.fetchTables();
        });

      }
    });

  }

  calculateTotalSeats() {
    let seatCount = 0;
    this.tables.forEach(table => {seatCount += table.maxSeats; });
    return seatCount;
  }

}
