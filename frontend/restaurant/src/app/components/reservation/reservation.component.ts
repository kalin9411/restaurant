import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Reservation from '../../models/reservation';
import * as moment from 'moment';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  constructor() { }

  @Input()
  reservation: Reservation;

  @Input()
  maxSeats: number;

  @Output()
  reservationRemoveEvent = new EventEmitter<object>();

  date: string;

  occupiedSeats: any[];
  freeSeats: any[];

  ngOnInit(): void {
    this.date = moment(this.reservation.date.split('Z')[0]).format('HH:mm  dddd Do MMM');
    this.occupiedSeats = new Array(this.reservation.numberOfClients);
    this.freeSeats = new Array(this.maxSeats - this.reservation.numberOfClients);
  }

  removeReservation() {
    this.reservationRemoveEvent.emit({reservationId: this.reservation.id});
  }

}
