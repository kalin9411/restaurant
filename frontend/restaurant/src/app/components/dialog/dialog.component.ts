import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import Table from '../../models/table';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { WorktimeService } from '../../services/worktime.service';
import { Workday } from '../../models/workDay';
import ReservationDTO from '../../models/reservationDTO';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Table[],
    public dialogRef: MatDialogRef<DialogComponent>,
    private readonly worktimeService: WorktimeService,
    ) {}

  @Input()
  ngxTimepicker: DialogComponent;

  seats: number[] = [];
  workTime: Workday[];
  date = new FormControl(new Date());

  workingHours = [];
  workingMinutes = [];

  selectedHour: string;
  selectedMinute: string;
  selectedDate: Workday;
  availableTables: Table[];
  selectedTable: Table;
  selectedName = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]);
  selectedPeople: number;


  ngOnInit(): void {

    const numOfMaxSeat = this.calculateMaxSeatNumber();
    for (let i = 0; i < numOfMaxSeat; i++) {
      this.seats.push(i + 1);
    }
    this.worktimeService.workingTime.subscribe(data => {
      this.workTime = data;
    });
    // tslint:disable-next-line: no-unused-expression
    this.dateChanged({value: new Date()}) as any;

  }

  getErrorMessage() {
    if (this.selectedName.hasError('required')) {
      return 'You must enter a name';
    }
    if (this.selectedName.hasError('minlength') || this.selectedName.hasError('maxlength')) {
      return 'Name Length must be between 3 and 30 characters';
    }
  }

  dateFilter = (date: Date)  => {
    const weekday = [];
    weekday[0] = 'Sunday';
    weekday[1] = 'Monday';
    weekday[2] = 'Tuesday';
    weekday[3] = 'Wednesday';
    weekday[4] = 'Thursday';
    weekday[5] = 'Friday';
    weekday[6] = 'Saturday';
    const dayName = weekday[date.getDay()];
    const dayNumber = date.getDate() + date.getMonth() * 100;
    const currentDay = this.workTime.find(day => day.name === dayName);
    return currentDay.isWorking && (new Date().getDate() + new Date().getMonth() * 100) <= dayNumber;
  }

  calculateMaxSeatNumber() {
    const newData = [...this.data];
    return newData.sort((a, b) => b.maxSeats - a.maxSeats)[0].maxSeats;
  }

  checkTableIsAvailAtTime(table: Table) {
    let result = true;
    let conflictedReservationTime: string;
    const selectedDate = moment(this.date.value).format().split('T')[0] + 'T' + this.selectedHour + ':' + this.selectedMinute + ':00.000Z';
    table.reservations.forEach(res => {
      const diff: number = moment.duration(moment(selectedDate).diff(moment(new Date(res.date)))).asMinutes();
      if (diff < 60 && diff > -60) {
        result = false;
        conflictedReservationTime = moment(res.date.split('Z')[0]).format('HH:mm');
      }

    });
    return {result, conflictedReservationTime};
  }


  dateChanged(event) {
    const weekday = [];
    weekday[0] = 'Sunday';
    weekday[1] = 'Monday';
    weekday[2] = 'Tuesday';
    weekday[3] = 'Wednesday';
    weekday[4] = 'Thursday';
    weekday[5] = 'Friday';
    weekday[6] = 'Saturday';
    const dayName = weekday[event.value.getDay()];
    const currentDay = this.workTime.find(day => day.name === dayName);
    this.selectedDate = currentDay;
    this.workingHours = [];

    for (let d = Number(this.selectedDate.startHour); d <= Number(this.selectedDate.endHour); d++) {

      if (d < 10) {
        this.workingHours.push(`0${d}`);
      } else {
      this.workingHours.push(`${d}`);
      }
    }
  }

  hourChanged(event) {
    const workMinutes: string[] = ['00', '15', '30', '45'];
    if (event === this.selectedDate.startHour) {
      const minuteIndex = workMinutes.findIndex(minute =>  minute === this.selectedDate.startMinute);
      this.workingMinutes = workMinutes.slice(minuteIndex, workMinutes.length);
      console.log(this.workingMinutes);
    } else {
      if (event === this.selectedDate.endHour) {
        const minuteIndex = workMinutes.findIndex(minute =>  minute === this.selectedDate.endMinute);
        this.workingMinutes = workMinutes.slice(0, minuteIndex + 1);
      } else {
      this.workingMinutes = workMinutes;
      }
    }
  }

  peopleChanged(event) {
    this.availableTables = this.data.filter(table => event <= table.maxSeats);
  }

  sendReservation() {

    const result: {table: Table, reservation: ReservationDTO} = {
      reservation: {
        name: this.selectedName.value,
        date: moment(this.date.value).format().split('T')[0] + 'T' + this.selectedHour + ':' + this.selectedMinute + ':00.000Z' as any,
        numberOfClients: this.selectedPeople,
      },
      table: this.selectedTable
    };
    this.dialogRef.close(result);
  }

}
