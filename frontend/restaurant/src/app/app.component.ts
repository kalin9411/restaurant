import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { TableService } from './services/table.service';
import { WorktimeService } from './services/worktime.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'restaurant';

  constructor(private readonly tableService: TableService, private readonly worktimeService: WorktimeService) { }

  ngOnInit() {
    this.tableService.fetchTables();
    this.worktimeService.fetchWorkingTime();
  }
}
