import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import ReservationDTO from '../models/reservationDTO';
import Table from '../models/table';
import { Workday } from '../models/workDay';


@Injectable({
  providedIn: 'root',
})
export class BackendService {
  private readonly url = 'http://localhost:3000';

  constructor(
    private http: HttpClient) {}

    getTables(): Observable<object> {
      console.log(`getting tables`);
      return this.http.get(`${this.url}/table`);
    }

    addReservation(table: Table, reservation: ReservationDTO): Observable<object> {
      console.log('adding reservation' + table.id);
      return this.http.post(`${this.url}/reservation/${table.id}`, reservation);
    }

    removeReservation(tableId: string): Observable<object> {
      return this.http.delete(`${this.url}/reservation/${tableId}`);
    }

    addTable(tableObj: {name: string, maxSeats: number}): Observable<object> {
      console.log('adding table');
      return this.http.post(`${this.url}/table`, tableObj);
    }

    updateTable(table: Table): Observable<object> {
      console.log('updating table ' + table.id);
      return this.http.put(`${this.url}/table/${table.id}`, table);
    }

    deleteTable(table: Table): Observable<object> {
      console.log('deleting table ' + table.id);
      return this.http.delete(`${this.url}/table/${table.id}`);
    }

    getWorkTime(): Observable<object> {
      return this.http.get(`${this.url}/workday/all`);
    }

    updateWorkTime(workday: Workday): Observable<object> {
      console.log('req update day ' + workday.name);
      return this.http.put(`${this.url}/workday/${workday.name}`, workday);
    }

}
