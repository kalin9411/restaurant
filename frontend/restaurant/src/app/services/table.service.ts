import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import ReservationDTO from '../models/reservationDTO';
import Table from '../models/table';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  private $tables = new BehaviorSubject<object>([]);

  constructor(private readonly backendService: BackendService) { }

  fetchTables() {
    this.backendService.getTables().subscribe(data => {
      this.$tables.next(data);
    });
  }

  addTable(table: {name: string, maxSeats: number}): Observable<object> {
    return this.backendService.addTable(table);
  }

  updateTable(table: Table): Observable<object> {
    return this.backendService.updateTable(table);
  }

  addReservation(table: Table, reservation: ReservationDTO): Observable<object> {
    return this.backendService.addReservation(table, reservation);
  }

  removeReservation(reservationId: string): Observable<object> {
    return this.backendService.removeReservation(reservationId);
  }

  deleteTable(table: Table): Observable<object> {
    return this.backendService.deleteTable(table);
  }

  public get tables(): Observable<Table[]> {
    return this.$tables.asObservable() as any;
  }
}
