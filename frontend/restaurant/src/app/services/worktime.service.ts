import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { Workday } from '../models/workDay';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root'
})
export class WorktimeService {

  private $worktime = new BehaviorSubject<object>([]);

  constructor(private readonly backendService: BackendService) { }

  fetchWorkingTime() {
    this.backendService.getWorkTime().subscribe(data => {
      this.$worktime.next(data);
    });
  }
  public get workingTime(): Observable<Workday[]> {
    return this.$worktime.asObservable() as any;
  }

  updateWorkingTime(workTimeObj: {
    Monday: Workday,
    Tuesday: Workday,
    Wednesday: Workday,
    Thursday: Workday,
    Friday: Workday,
    Saturday: Workday,
    Sunday: Workday
  } ) {
    const requestArray = [];

    Object.keys(workTimeObj).forEach(key => {
      requestArray.push(this.backendService.updateWorkTime(workTimeObj[key]));
    });
    return forkJoin(requestArray).subscribe(data => {
      console.log('Saved');
    });
  }

}
