import Reservation from './reservation';

export default class Table {

  id: string;

  name: string;

  maxSeats: number;

  reservations: Reservation[];


}
