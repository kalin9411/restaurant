export class Workday {

  isWorking: boolean;

  name: string;

  startHour: string;

  startMinute: string;

  endHour: string;
  
  endMinute: string;
}
