
export default class ReservationDTO {

  name: string;

  date: Date;

  numberOfClients: number;

}
