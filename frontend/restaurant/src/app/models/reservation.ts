import Table from './table';

export default class Reservation {

  id: string;

  name: string;

  date: string;

  table: Table;

  numberOfClients: number;

}
